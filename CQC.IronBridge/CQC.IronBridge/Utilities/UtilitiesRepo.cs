﻿using CQC.IronBridge.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CQC.IronBridge.Utilities
{
    public static class UtilitiesRepo
    {
        public static string ReturnIconClass(string helper)
        {
            IconEnum temp = IconEnum.None;
            if (Enum.TryParse(helper, out temp))
            {
                switch (temp)
                {
                    case IconEnum.Check:
                        return "fas fa-check";
                    case IconEnum.CheckCircle:
                        return "far fa-check-circle";
                    case IconEnum.Times:
                        return "fas fa-times";
                    case IconEnum.TimesCircle:
                        return "far fa-times-circle";
                    default:
                        return string.Empty;
                }
            }

            return string.Empty;
        }
    }
}