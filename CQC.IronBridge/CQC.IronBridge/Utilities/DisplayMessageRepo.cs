﻿using CQC.IronBridge.Entities;
using CQC.IronBridge.Models;

namespace CQC.IronBridge.Utilities
{
    public static class DisplayMessageRepo
    {
        public static DisplayMessage ReturnDisplayMessage(ThemeEnum displayType, bool includesIcon, IconEnum icon, string text, bool isClosable = true)
        {
            return new DisplayMessage()
            {
                MessageType = displayType,
                IncludesIcon = includesIcon,
                Icon = icon,
                Text = text,
                IsClosable = isClosable
            };
        }
    }
}