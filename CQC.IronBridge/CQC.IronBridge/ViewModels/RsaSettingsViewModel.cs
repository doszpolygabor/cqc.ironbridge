﻿namespace CQC.IronBridge.ViewModels
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class RsaSettingsViewModel
    {

        public List<SelectListItem> AvailablePorts { get; set; }

        public int Port { get; set; }

        public List<SelectListItem> ParityList { get; set; }

        public string Parity { get; set; }

        public List<SelectListItem> BitRateList { get; set; }

        public int BitRate { get; set; }

        public List<SelectListItem> StopBitsList { get; set; }

        public int StopBits { get; set; }

        public List<SelectListItem> HashOptions { get; set; }

        public string HashType { get; set; }

        public List<SelectListItem> ModulusList { get; set; }

        public int Modulus { get; set; }

        public List<SelectListItem> MaxDataOptions { get; set; }

        public int MaxDataOctets { get; set; }

        public string ShortDescription
        {
            get
            {
                return $"OAEP: {this.HashType}, Modulus: {this.Modulus}, Max Data (octets):{this.MaxDataOctets}";
            }
        }
    }
}