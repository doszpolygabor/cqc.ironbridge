﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CQC.IronBridge.ViewModels
{
    public class AesKeyViewModel
    {
        public AesSettingsViewModel Settings { get; set; }

        public string Key { get; set; }
    }
}