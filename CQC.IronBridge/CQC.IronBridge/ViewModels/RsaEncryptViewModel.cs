﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CQC.IronBridge.ViewModels
{
    public class RsaEncryptViewModel
    {
        public RsaSettingsViewModel Settings { get; set; }

        public string TextToEncrypt { get; set; }

        public string Cipher { get; set; }

        public bool UsePrivateKey { get; set; }
    }
}