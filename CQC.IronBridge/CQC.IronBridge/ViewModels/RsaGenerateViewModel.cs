﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CQC.IronBridge.ViewModels
{
    public class RsaGenerateViewModel
    {
        public RsaSettingsViewModel Settings { get; set; }

        public string PrivateKey { get; set; }

        public string X509Extension { get; set; }
    }
}