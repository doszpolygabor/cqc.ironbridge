﻿using System.ComponentModel.DataAnnotations;

namespace CQC.IronBridge.ViewModels
{
    public class AesEncryptViewModel
    {
        public string UploadFile { get; set; }

        public string SampleOutput { get; set; }

        public AesSettingsViewModel Settings { get; set; }
        [Required]
        public string UploadedFile { get; set; }

        [Required]
        public string FileDestination { get; set; }

        public bool EncryptionSuccessful { get; set; }

        public string OutputPreview { get; set; }
    }
}