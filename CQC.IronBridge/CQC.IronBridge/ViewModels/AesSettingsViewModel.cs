﻿namespace CQC.IronBridge.ViewModels
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class AesSettingsViewModel
    {
        public List<SelectListItem> AvailablePorts { get; set; }
        
        public int Port { get; set; }

        public List<SelectListItem> ParityList { get; set; }

        public string Parity { get; set; }

        public List<SelectListItem> BitRateList { get; set; }
        
        public int BitRate { get; set; }

        public List<SelectListItem> StopBitsList { get; set; }

        public int StopBits { get; set; }

        public List<SelectListItem> KeyLenghtOptions { get; set; }

        public int KeyLength { get; set; }

        public bool UseIV { get; set; }

        public bool UnicodeEnabled { get; set; }

        public string ShortDescription
        {
            get
            {
                return $"{this.KeyLength}, {(this.UnicodeEnabled ? "Unicode, " : "")}IV:{(this.UseIV ? "On" : "Off")}";
            }
        }
    }
}