﻿using System;
using System.Security.Cryptography;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DependencyResolver = CQC.IronBridge.Services.DependencyResolver;

namespace CQC.IronBridge
{
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Gets the dependency resolver.
        /// </summary>
        /// <value>The dependency resolver.</value>
        public static DependencyResolver DependencyResolver { get; } = new DependencyResolver();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ControllerBuilder.Current.SetControllerFactory(new DefaultControllerFactory(DependencyResolver));
        }
    }
}
