﻿using CQC.IronBridge.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CQC.IronBridge.Models
{
    public class DisplayMessage
    {
        public ThemeEnum MessageType { get; set; }
        public string Text { get; set; }
        public bool IsClosable { get; set; }
        public bool IncludesIcon { get; set; }
        public IconEnum Icon { get; set; }

        public string IconClass
        {
            get
            {
                return Utilities.UtilitiesRepo.ReturnIconClass(Icon.ToString());
            }
        }
    }
}