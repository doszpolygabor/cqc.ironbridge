﻿namespace CQC.IronBridge.Entities
{
    public enum ThemeEnum
    {
        None = 0,
        Primary = 1,
        Secondary = 2,
        Success = 3,
        Info = 4,
        Warning = 5,
        Danger = 6,
        Light = 7,
        Dark = 8,
        Slate = 9
    }
}