﻿namespace CQC.IronBridge.Entities
{
    public enum IconEnum
    {
        None = 0,
        Check = 1,
        CheckCircle = 2,
        Times = 3,
        TimesCircle = 4
    }
}