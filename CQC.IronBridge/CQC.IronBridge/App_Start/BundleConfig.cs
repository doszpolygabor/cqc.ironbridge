﻿using System.Web;
using System.Web.Optimization;

namespace CQC.IronBridge
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            // Bootstrap framework bundles
            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                "~/Content/bootstrap.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/CQCTheme").Include(
                    "~/Content/Scss/IronBridge/Base/base.css",
                    "~/Content/theme.css"));

            // Slidebars bundles
            bundles.Add(new StyleBundle("~/Content/slidebars").Include(
                "~/Content/slidebars.min.css"));
            bundles.Add(new ScriptBundle("~/bundles/slidebars").Include(
                "~/Scripts/slidebars.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/portal").Include(
                "~/Scripts/portal.js"));
        }
    }
}
