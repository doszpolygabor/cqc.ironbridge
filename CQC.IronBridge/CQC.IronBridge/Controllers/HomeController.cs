﻿using CQC.IronBridge.Entities;
using CQC.IronBridge.Models;
using System;
using System.Web.Mvc;

namespace CQC.IronBridge.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                this.ViewBag.DisplayMessage = Utilities.DisplayMessageRepo.ReturnDisplayMessage(ThemeEnum.Success, true, IconEnum.Check, "This is an example display message.");

                return View();
            }
            catch (Exception ex)
            {
                throw new Exception("", ex);
            }
        }

        [HttpGet]
        public ActionResult About()
        {
            try
            {
                return View();
            }
            catch(Exception ex)
            {
                throw new Exception("", ex);
            }
            
        }

        [HttpGet]
        public ActionResult Contact()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception("", ex);
            }
        }

        [HttpGet]
        [Route("file/type")]
        public ActionResult SelectFileEncryptionType()
        {
            try
            {
                return View("SelectType");
            }
            catch (Exception ex)
            {
                throw new Exception("", ex);
            }           
        }

        [HttpGet]
        public ActionResult PageNotFound()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception("", ex);
            }
        }

        [HttpGet]
        public ActionResult Error()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception("", ex);
            }
        }

        [HttpGet]
        public ActionResult Maintenance()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception("", ex);
            }
        }

        [HttpGet]
        public ActionResult NoScript()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception("", ex);
            }
        }
    }
}