﻿using CQC.IronBridge.Clients.Interfaces;
using CQC.IronBridge.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CQC.IronBridge.Controllers
{
    [RoutePrefix("file/aes")]
    public class AesFileEncryptionController : Controller
    {
        private readonly IFileEncryptClient fileEncryptClient;

        public AesFileEncryptionController(IFileEncryptClient fileEncryptClient)
        {
            this.fileEncryptClient = fileEncryptClient;
        }

        [Route]
        public ActionResult Init()
        {
            return this.View();
        }

        [Route("key")]
        public ActionResult Key()
        {
            var newkeyValue = this.TempData["NewKey"]?.ToString();

            var model = new AesKeyViewModel
            {
                Key = newkeyValue,
                Settings = new AesSettingsViewModel
                {
                    BitRate = 11520,
                    KeyLength = 128,
                    Parity = "even",
                    Port = 3,
                    StopBits = 1,
                    UnicodeEnabled = true,
                    UseIV = false
                }
            };

            return this.View(model);
        }

        [Route("key")]
        [HttpPost]
        public ActionResult Key(AesSettingsViewModel model)
        {

            // todo: call api to save key or whatever

            return this.RedirectToAction("encrypt");
        }

        [Route("getnewkey")]
        public ActionResult GenerateKey()
        {
            var key = Guid.NewGuid();
            this.TempData["NewKey"] = key;

            return this.RedirectToAction("key");
        }

        [HttpGet]
        [Route("encrypt")]
        public ActionResult Encrypt()
        {
            var model = new AesEncryptViewModel
            {
                Settings = new AesSettingsViewModel
                {
                    BitRate = 11520,
                    KeyLength = 128,
                    Parity = "even",
                    Port = 3,
                    StopBits = 1,
                    UnicodeEnabled = true,
                    UseIV = false
                }
            };

            this.Session.Clear();

            return this.View(model);
        }

        [Route("encrypt")]
        [HttpPost]
        public ActionResult Encrypt(AesEncryptViewModel model)
        {
            model.Settings = new AesSettingsViewModel
            {
                BitRate = 11520,
                KeyLength = 128,
                Parity = "even",
                Port = 3,
                StopBits = 1,
                UnicodeEnabled = true,
                UseIV = false
            };

            if (this.Request.Files != null && this.Request.Files.Count == 1 && this.Request.Files[0].ContentLength > 0)
            {
                try
                {
                    using (var binaryReader = new BinaryReader(this.Request.Files[0].InputStream, new UTF8Encoding(), true))
                    {
                        var fileData = binaryReader.ReadBytes(this.Request.Files[0].ContentLength);
                        model.SampleOutput = Encoding.UTF8.GetString(fileData);

                        

                        var fileId = Guid.NewGuid().ToString();
                        this.Session[fileId] = fileData;

                        model.UploadFile = fileId;
                    }
                }
                catch (Exception ex)
                {
                    
                    // todo: log error and error display message.
                }
            }
            else
            {
                // todo: show display message.
            }

            return View(model);
        }

        [Route("downloadfile/{id}")]
        [HttpGet]
        public ActionResult DownloadEncryptedFile(string id)
        {
            var fileBytes = (byte[])this.Session[id];
            if (fileBytes == null || fileBytes.Length == 0)
            {
                var model = new AesEncryptViewModel
                {
                    Settings = new AesSettingsViewModel
                    {
                        BitRate = 11520,
                        KeyLength = 128,
                        Parity = "even",
                        Port = 3,
                        StopBits = 1,
                        UnicodeEnabled = true,
                        UseIV = false
                    }
                };

                //todo: add display message: missing encrypted file.

                return this.View("Encrypt", model);
            }

            string fileName = $"{id}.txt";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [Route("settings")]
        public ActionResult Settings()
        {
            // todo: api call to return settings and available options

            var model = new AesSettingsViewModel
            {
                AvailablePorts = new List<SelectListItem>
                {
                    new SelectListItem { Text = "1", Value = "1" },
                    new SelectListItem { Text = "2", Value = "2" },
                    new SelectListItem { Text = "3", Value = "3" },
                    new SelectListItem { Text = "4", Value = "4" }
                },
                BitRateList = new List<SelectListItem>
                {
                    new SelectListItem { Text = "9600", Value = "9600" },
                    new SelectListItem { Text = "11520", Value = "11520" }
                },
                KeyLenghtOptions = new List<SelectListItem>
                {
                    new SelectListItem { Text = "AES 128", Value = "128" },
                    new SelectListItem { Text = "AES 256", Value = "256" },
                    new SelectListItem { Text = "AES 512", Value = "512" }
                },
                StopBitsList = new List<SelectListItem>
                {
                    new SelectListItem { Text = "1", Value = "1" },
                    new SelectListItem { Text = "2", Value = "2" },
                    new SelectListItem { Text = "3", Value = "3" },
                    new SelectListItem { Text = "4", Value = "4" }
                },
                ParityList = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Even", Value = "even" }
                },
                BitRate = 11520,
                KeyLength = 128,
                Parity = "even",
                Port = 3,
                StopBits = 1,
                UnicodeEnabled = true,
                UseIV = false
            };

            return this.View(model);
        }

        [Route("settings")]
        [HttpPost]
        public ActionResult Settings(AesSettingsViewModel model)
        {
            // todo: add display message of success or fail

            // todo: api call to save settings

            return this.RedirectToAction("settings");
        }

        [Route("resettodefaults")]
        public ActionResult Restore()
        {
            // todo: add display message of success or fail

            // todo: api call to reset defaults

            ModelState.Clear();

            return this.RedirectToAction("settings");
        }
    }
}