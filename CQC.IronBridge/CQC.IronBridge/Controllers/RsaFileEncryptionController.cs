﻿namespace CQC.IronBridge.Controllers
{
    using CQC.IronBridge.ViewModels;
    using System.Collections.Generic;
    using System.Web.Mvc;

    [RoutePrefix("file/rsa")]
    public class RsaFileEncryptionController : Controller
    {
        [Route]
        public ActionResult Init()
        {
            return this.View();
        }

        [Route("generate")]
        public ActionResult Generate()
        {
            var model = new RsaGenerateViewModel
            {
                Settings = new RsaSettingsViewModel
                {
                    BitRate = 11520,
                    HashType = "SHA1",
                    Parity = "even",
                    Port = 3,
                    StopBits = 1,
                    MaxDataOctets = 86,
                    Modulus = 1024
                },
                PrivateKey = "sample private key",
                X509Extension = null
            };
            return this.View(model);
        }

        [Route("encrypt")]
        public ActionResult Encrypt()
        {
            return this.View();
        }

        [Route("settings")]
        public ActionResult Settings()
        {
            // todo: api call to return settings and available options

            var model = new RsaSettingsViewModel
            {
                AvailablePorts = new List<SelectListItem>
                {
                    new SelectListItem { Text = "1", Value = "1" },
                    new SelectListItem { Text = "2", Value = "2" },
                    new SelectListItem { Text = "3", Value = "3" },
                    new SelectListItem { Text = "4", Value = "4" }
                },
                BitRateList = new List<SelectListItem>
                {
                    new SelectListItem { Text = "9600", Value = "9600" },
                    new SelectListItem { Text = "11520", Value = "11520" }
                },
                HashOptions = new List<SelectListItem>
                {
                    new SelectListItem { Text = "SHA1", Value = "SHA1" }
                },
                StopBitsList = new List<SelectListItem>
                {
                    new SelectListItem { Text = "1", Value = "1" },
                    new SelectListItem { Text = "2", Value = "2" },
                    new SelectListItem { Text = "3", Value = "3" },
                    new SelectListItem { Text = "4", Value = "4" }
                },
                ParityList = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Even", Value = "even" }
                },
                MaxDataOptions = new List<SelectListItem>
                {
                    new SelectListItem { Text = "85", Value = "85" },
                    new SelectListItem { Text = "86", Value = "86" }
                },
                ModulusList = new List<SelectListItem>
                {
                    new SelectListItem { Text = "256", Value = "256" },
                    new SelectListItem { Text = "512", Value = "512" },
                    new SelectListItem { Text = "1024", Value = "1024" }
                },
                BitRate = 11520,
                HashType = "SHA1",
                Parity = "even",
                Port = 3,
                StopBits = 1,
                MaxDataOctets = 86,
                Modulus = 1024
            };

            return this.View(model);
        }

        [Route("settings")]
        [HttpPost]
        public ActionResult Settings(RsaSettingsViewModel model)
        {
            // todo: add display message of success or fail

            // todo: api call to save settings

            return this.RedirectToAction("settings");
        }

        [Route("resettodefaults")]
        public ActionResult Restore()
        {
            // todo: add display message of success or fail

            // todo: api call to reset defaults

            ModelState.Clear();

            return this.RedirectToAction("settings");
        }
    }
}