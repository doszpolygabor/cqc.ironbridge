﻿using CQC.IronBridge.Clients.Interfaces;
using CQC.IronBridge.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace CQC.IronBridge.Services
{
    public class ApiConnectorService : IApiConnectorService
    {
        public string WebAPIUrl => WebConfigurationManager.AppSettings["APIAddress"].ToString();

        public string LogFilePath => WebConfigurationManager.AppSettings["LogFile"].ToString();

        public LogLevel LoggingLevel => string.IsNullOrEmpty(WebConfigurationManager.AppSettings["LogLevel"].ToString()) ? LogLevel.Error : (LogLevel)Enum.Parse(typeof(LogLevel), WebConfigurationManager.AppSettings["LogLevel"].ToString());
    }
}