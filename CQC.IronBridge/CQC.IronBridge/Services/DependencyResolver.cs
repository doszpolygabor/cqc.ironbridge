﻿using CQC.IronBridge.Clients.Clients;
using CQC.IronBridge.Clients.Interfaces;
using CQC.IronBridge.Common.Interfaces;
using CQC.IronBridge.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Unity;
using Unity.Exceptions;
using Unity.Lifetime;

namespace CQC.IronBridge.Services
{
    /// <summary>
    /// Implementation of the dependency resolver.
    /// </summary>
    public class DependencyResolver : IControllerActivator, IDisposable
    {
        /// <summary>
        /// The global container.
        /// </summary>
        private UnityContainer globalContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyResolver"/> class.
        /// </summary>
        public DependencyResolver()
        {
            this.globalContainer = new UnityContainer();
            this.RegisterServices(this.globalContainer);
            this.RegisterControllers(this.globalContainer);
        }

        /// <summary>
        /// Gets the global container.
        /// </summary>
        public UnityContainer GlobalContainer
        {
            get { return this.globalContainer; }
        }

        /// <summary>
        /// Creates an instance of controller with the specified type.
        /// </summary>
        /// <param name="requestContext">The request context.</param>
        /// <param name="controllerType">Type of the controller.</param>
        /// <returns>The created controller.</returns>
        public IController Create(RequestContext requestContext, Type controllerType)
        {
            try
            {
                return this.globalContainer.Resolve(controllerType) as IController;
            }
            catch (ResolutionFailedException ex)
            {
                // Must return null if the service is not resolved.
                return null;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            if (this.globalContainer == null)
            {
                return;
            }

            this.globalContainer.Dispose();
            this.globalContainer = null;
        }

        /// <summary>
        /// Registers the services.
        /// </summary>
        /// <param name="container">The container.</param>
        private void RegisterServices(IUnityContainer container)
        {
            // Please, maintain alphabetical order
            container.RegisterType<IApiConnectorService, ApiConnectorService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IFileEncryptClient, FileEncryptClient>(new ContainerControlledLifetimeManager());
            container.RegisterType<ILoggingService, LoggingService>(new ContainerControlledLifetimeManager());
        }

        /// <summary>
        /// Registers the controllers.
        /// </summary>
        /// <param name="container">The container.</param>
        private void RegisterControllers(IUnityContainer container)
        {
            // Please, maintain alphabetical order
            container.RegisterType<AesFileEncryptionController>();
            container.RegisterType<HomeController>();
            container.RegisterType<RsaFileEncryptionController>();
        }
    }
}