﻿using CQC.IronBridge.Clients.Interfaces;
using CQC.IronBridge.Common.Entities;
using CQC.IronBridge.Common.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CQC.IronBridge.Services
{
    public class LoggingService : ILoggingService
    {
        private readonly IApiConnectorService apiConnectorService;

        /// <summary>
        /// The log file path
        /// </summary>
        private readonly string logFilePath;

        /// <summary>
        /// The queue
        /// </summary>
        private BlockingCollection<string> queue;

        public LoggingService(IApiConnectorService apiConnectorService)
        {
            this.apiConnectorService = apiConnectorService;
            if (!string.IsNullOrEmpty(this.apiConnectorService.LogFilePath))
            {
                this.logFilePath = this.apiConnectorService.LogFilePath;
            }
            else
            {
                this.logFilePath = "log.txt";
            }

            this.queue = new BlockingCollection<string>(1000);

            Task.Run(() =>
            {
                foreach (var item in this.queue.GetConsumingEnumerable())
                {
                    this.DoLog(item);
                }
            });
        }

        /// <summary>
        /// Logs the information.
        /// </summary>
        /// <param name="message">The message.</param>
        public void LogInformation(string message)
        {
            Debug.WriteLine($"{DateTime.Now:0:HH:mm:ss.fff} I {message}");
            if (this.apiConnectorService.LoggingLevel >= LogLevel.Info)
            {
                this.queue.Add($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs the debug information.
        /// </summary>
        /// <param name="message">The message to log.</param>
        public void LogDebug(string message)
        {
            Debug.WriteLine($"{DateTime.Now:0:HH:mm:ss.fff} D {message}");
            if (this.apiConnectorService.LoggingLevel >= LogLevel.Info)
            {
                this.queue.Add($"DEBUG: {message}");
            }
        }

        /// <summary>
        /// Logs the warning.
        /// </summary>
        /// <param name="message">The message.</param>
        public void LogWarning(string message)
        {
            Debug.WriteLine($"{DateTime.Now:0:HH:mm:ss.fff} W {message}");
            if (this.apiConnectorService.LoggingLevel >= LogLevel.Info)
            {
                this.queue.Add($"WARNING: {message}");
            }
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="message">The message.</param>
        public void LogError(string message)
        {
            Debug.WriteLine($"{DateTime.Now:0:HH:mm:ss.fff} E {message}");
            if (this.apiConnectorService.LoggingLevel >= LogLevel.Info)
            {
                this.queue.Add($"ERROR: {message}");
            }
        }

        /// <summary>
        /// Does the log.
        /// </summary>
        /// <param name="message">The message.</param>
        private void DoLog(string message)
        {
            using (var w = File.AppendText(this.logFilePath))
            {
                w.WriteLine($"{message} - {DateTime.Now.ToString()}");
            }
        }
    }
}