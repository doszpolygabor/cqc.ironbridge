﻿function getCircleLength(el) {
    'use strict';
    var r = $(el).attr('r'),
        circleLength = 2 * Math.PI * r;

    return circleLength;
}

function calcStrokePercentage(num, strokeLength) {
    'use strict';
    var x = strokeLength,
        y = num;

    return x - (y / 100 * x);
}

function displayStroke(id, num) {
    'use strict';
    var gs = id.find('.guage-stroke'),
        strokeLength = getCircleLength(gs);

    gs.css({
        'stroke-dasharray': strokeLength + ' ' + strokeLength,
        'stroke-dashoffset': "-" + calcStrokePercentage(num, strokeLength),
        'stroke-opacity': '1'
    });

    var count = 0;
    var interval = setInterval(function () {

        if (count == num) {
            clearInterval(interval);
        }
        else {

            count++;
        }

        $(".progress-display__perc").html(count);
    }, 2000 / num);
}

function ToggleCollapse(target) {

    var $this = $(target),
        aria = $this.attr('aria-expanded'),
        icon = $this.find('i');

    if (aria !== undefined && aria !== "") {
        icon.attr('class', (aria.toLowerCase() === "true" ? 'fas fa-caret-down' : 'fas fa-caret-up'));
    }
}

$(function () {

    'use strict';

    $(window).resize(function () {
        var footerHeight = $('footer').outerHeight();
        $('.push').height(footerHeight);
        $('.page-wrapper').css({ 'marginBottom': '-' + footerHeight + 'px' });
    });
    $(window).resize();

    /******************************************************
	SLIDEBARS DEVICE MENUS
	******************************************************/

    var controller = new slidebars(),
        primaryMenuTrigger = $('#PrimaryMenuTrigger'),
        activeMenu;

    controller.init();

    primaryMenuTrigger.on('click', function (event) {
        event.stopPropagation();
        event.preventDefault();
        controller.toggle('primary');
    });

    $(document).on('click', '.js-close-any', function (event) {
        if (controller.getActiveSlidebar()) {
            event.preventDefault();
            event.stopPropagation();
            if (controller.isActiveSlidebar('primary')) {
                primaryMenuTrigger.removeClass('is-open');
            }
            controller.close();
        }
    });

    $(controller.events).on('opening', function () {
        $('[canvas]').addClass('js-close-any device-menu--is-open');
        activeMenu = controller.getActiveSlidebar();
        if (controller.isActiveSlidebar('primary')) {
            primaryMenuTrigger.addClass('is-open');
        }

    });

    $(controller.events).on('closing', function () {
        $('[canvas]').removeClass('js-close-any device-menu--is-open');
        if (activeMenu !== "" && activeMenu.toLowerCase() === 'primary') {
            primaryMenuTrigger.removeClass('is-open');
        }
    });

    /******************************************************
	SIDEBAR NAVIGATION
	******************************************************/

    var sidebar = $('.sidebar-nav');
    if (sidebar.length) {
        sidebar.find('.list-item.sub').each(function () {
            $(this).on('click', function () {
                $(this).toggleClass('open');
            });
        });
    }
});