﻿using CQC.IronBridge.Entities;
using CQC.IronBridge.Utilities;
using System;
using System.Web.Mvc;

namespace CQC.IronBridge.Extensions
{
    public static class HtmlExtensions
    {
        public static string ReturnAlertClass(this string helper)
        {
            ThemeEnum temp = ThemeEnum.Primary;
            if (Enum.TryParse(helper, out temp))
            {
                return "alert-" + temp.ToString().ToLower();
            }

            return string.Empty;
        }

        public static string ReturnIconClass(this string helper)
        {
            return UtilitiesRepo.ReturnIconClass(helper);
        }
    }
}