﻿using CQC.IronBridge.Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQC.IronBridge.Clients.Interfaces
{
    public interface IApiConnectorService
    {
        /// <summary>
        /// Gets the web api url
        /// </summary>
        /// <value>The url for the web api</value>
        string WebAPIUrl { get; }

        /// <summary>
        /// Gets the log file path.
        /// </summary>
        /// <value>
        /// The log file path.
        /// </value>
        string LogFilePath { get; }

        /// <summary>
        /// Gets the logging level.
        /// </summary>
        LogLevel LoggingLevel { get; }
    }
}
