﻿using CQC.IronBridge.Common.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CQC.IronBridge.Clients.Interfaces
{
    public interface IFileEncryptClient
    {
        Task<NetworkResponse<AesEncryptResponse>> AesEncryptAsync(
            string textToEncrypt,
            string key);

        Task<NetworkResponse<AesGenerateKeyResponse>> AesGenerateKeyAsync();

        Task<NetworkResponse<RsaEncryptResponse>> RsaEncryptAsync(
            string textToEncrypt,
            bool usePrivateKey);

        Task<NetworkResponse<RsaDecryptResponse>> RsaDecryptAsync(
            string cipherText,
            bool usePrivateKey);

        Task<NetworkResponse<RsaGenerateExtensionResponse>> RsaGenerateExtensionAsync();
    }
}
