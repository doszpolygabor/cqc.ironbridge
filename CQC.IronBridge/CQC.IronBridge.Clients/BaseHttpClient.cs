﻿namespace CQC.IronBridge.Clients
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Threading;
    using System.Threading.Tasks;
    using CQC.IronBridge.Common.Interfaces;

    /// <summary>
    /// Implementation of the base HTTP client.
    /// </summary>
    public abstract class BaseHttpClient : IDisposable
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private readonly ILoggingService loggingService;

        /// <summary>
        /// The HTTP client.
        /// </summary>
        private readonly HttpClient httpClient;

        /// <summary>
        /// The media type formatter.
        /// </summary>
        private readonly MediaTypeFormatter mediaTypeFormatter;

        /// <summary>
        /// The media type formatters.
        /// </summary>
        private readonly MediaTypeFormatter[] mediaTypeFormatters;

        /// <summary>
        /// The request ID.
        /// </summary>
        private long requestId;

        /// <summary>
        /// A value indicating whether this instance has been disposed.
        /// </summary>
        private bool isDisposed;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseHttpClient" /> class.
        /// </summary>
        /// <param name="loggingService">The logging service.</param>
        /// <param name="mediaTypeFormatter">The media type formatter.</param>
        protected BaseHttpClient(ILoggingService loggingService, MediaTypeFormatter mediaTypeFormatter)
        {
            this.loggingService = loggingService;

            this.httpClient = new HttpClient();
            this.mediaTypeFormatter = mediaTypeFormatter;
            this.mediaTypeFormatters = new[] { this.mediaTypeFormatter };
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Gets the API request.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="requestUri">The request URI.</param>
        /// <returns>The HTTP request message.</returns>
        protected abstract HttpRequestMessage GetApiRequest(HttpMethod method, Uri requestUri);

        /// <summary>
        /// Gets the API request.
        /// </summary>
        /// <typeparam name="T">The type of the content.</typeparam>
        /// <param name="method">The method.</param>
        /// <param name="requestUri">The request URI.</param>
        /// <param name="content">The content.</param>
        /// <returns>The HTTP request message.</returns>
        protected HttpRequestMessage GetApiRequest<T>(HttpMethod method, Uri requestUri, T content)
        {
            var request = this.GetApiRequest(method, requestUri);
            request.Content = new ObjectContent<T>(content, this.mediaTypeFormatter);
            return request;
        }

        /// <summary>
        /// Gets the network response from the specified HTTP request asynchronously.
        /// </summary>
        /// <param name="request">The HTTP request.</param>
        /// <returns>The task returning the network response.</returns>
        protected async Task<NetworkResponse> GetNetworkResponseAsync(HttpRequestMessage request)
        {
            HttpResponseMessage response = null;
            var activityId = this.RegisterActivity(request?.RequestUri);

            try
            {
                response = await this.httpClient.SendAsync(request).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                this.loggingService.LogWarning($"CS: Error getting network response from {request?.RequestUri.OriginalString}. Error: {ex.Message}");

                return null;
            }
            finally
            {
                var statusCode = response?.StatusCode.ToString();
                this.UnregisterActivity(activityId, statusCode);
            }

            return response.ToNetworkResponse();
        }

        /// <summary>
        /// Gets the network response from the specified HTTP request asynchronously.
        /// </summary>
        /// <typeparam name="TContent">The type of the content.</typeparam>
        /// <param name="request">The HTTP request.</param>
        /// <returns>The task returning the network response.</returns>
        protected async Task<NetworkResponse<TContent>> GetNetworkResponseAsync<TContent>(HttpRequestMessage request)
            where TContent : class
        {
            NetworkResponse<TContent> networkResponse = null;
            var activityId = this.RegisterActivity(request?.RequestUri);

            try
            {
                var response = await this.httpClient.SendAsync(request).ConfigureAwait(false);
                if (response == null)
                {
                    return null;
                }

                if (response.Content == null)
                {
                    networkResponse = response.ToNetworkResponse(default(TContent));
                }
                else
                {
                    TContent content;
                    if (typeof(TContent) == typeof(string))
                    {
                        content = await response.Content.ReadAsStringAsync().ConfigureAwait(false) as TContent;
                    }
                    else if (typeof(TContent) == typeof(Stream))
                    {
                        content = await response.Content.ReadAsStreamAsync().ConfigureAwait(false) as TContent;
                    }
                    else
                    {
                        content = await response.Content.ReadAsAsync<TContent>(this.mediaTypeFormatters).ConfigureAwait(false);
                    }

                    networkResponse = response.ToNetworkResponse(content);
                }
            }
            catch (Exception ex)
            {
                this.loggingService.LogWarning($"CS: Error getting network response from {request?.RequestUri.OriginalString}. Error: {ex.Message}");

                return null;
            }
            finally
            {
                var statusCode = networkResponse?.StatusCode.ToString();
                this.UnregisterActivity(activityId, statusCode);
            }

            return networkResponse;
        }

        /// <summary>
        /// Gets the successful network response with the specified content.
        /// </summary>
        /// <typeparam name="T">The type of the content.</typeparam>
        /// <param name="content">The content.</param>
        /// <returns>The successful network response with the specified content.</returns>
        protected NetworkResponse<T> GetSuccessfulNetworkResponse<T>(T content)
            where T : class
        {
            return new NetworkResponse<T>(true, HttpStatusCode.OK, NetworkResponseStatus.Success, HttpStatusCode.OK.ToString(), content);
        }

        /// <summary>
        /// Registers the service activity.
        /// </summary>
        /// <param name="requestUri">The request URI.</param>
        /// <returns>The activity ID.</returns>
        private long RegisterActivity(Uri requestUri)
        {
            Interlocked.Increment(ref this.requestId);
            this.loggingService.LogInformation($"CS: Network request {this.requestId} to {requestUri.OriginalString}");
            return this.requestId;
        }

        /// <summary>
        /// Unregisters the service activity.
        /// </summary>
        /// <param name="activityId">The activity ID.</param>
        /// <param name="statusCode">The HTTP status code.</param>
        private void UnregisterActivity(long activityId, string statusCode)
        {
            this.loggingService.LogInformation($"CS: Network request {activityId} completed with status code {statusCode}");
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (this.isDisposed)
            {
                return;
            }

            if (disposing)
            {
                this.httpClient.Dispose();
            }

            this.isDisposed = true;
        }

        #endregion Methods
    }
}
