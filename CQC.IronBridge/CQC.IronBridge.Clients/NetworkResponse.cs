﻿namespace CQC.IronBridge.Clients
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents the network response returning the status only.
    /// </summary>
    public class NetworkResponse
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkResponse"/> class.
        /// </summary>
        /// <param name="isSuccessful">if set to <c>true</c> the response is successful.</param>
        /// <param name="statusCode">The status code.</param>
        /// <param name="status">The status.</param>
        /// <param name="statusPhrase">The status phrase.</param>
        public NetworkResponse(
            bool isSuccessful,
            HttpStatusCode statusCode,
            NetworkResponseStatus status,
            string statusPhrase)
        {
            this.IsSuccessful = isSuccessful;
            this.StatusCode = statusCode;
            this.Status = status;
            this.StatusPhrase = statusPhrase;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets a value indicating whether the response is successful.
        /// </summary>
        /// <value><c>true</c> if the response is successful; otherwise, <c>false</c>.</value>
        public bool IsSuccessful { get; }

        /// <summary>
        /// Gets the status phrase.
        /// </summary>
        /// <value>The status phrase.</value>
        public HttpStatusCode StatusCode { get; }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>The status.</value>
        public NetworkResponseStatus Status { get; }

        /// <summary>
        /// Gets the status phrase.
        /// </summary>
        /// <value>The status phrase.</value>
        public string StatusPhrase { get; }

        #endregion Properties
    }
}
