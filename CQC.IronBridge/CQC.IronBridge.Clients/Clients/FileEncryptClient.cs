﻿using CQC.IronBridge.Clients.Interfaces;
using CQC.IronBridge.Common.Dtos;
using CQC.IronBridge.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace CQC.IronBridge.Clients.Clients
{
    public class FileEncryptClient : BaseHttpClient, IFileEncryptClient
    {
        #region Fields

        /// <summary>
        /// The client information service.
        /// </summary>
        private readonly IApiConnectorService apiConnectorService;

        /// <summary>
        /// The API hostname.
        /// </summary>
        private readonly string baseUrl;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationClient"/> class.
        /// </summary>
        /// <param name="apiConnectorService">The api connect information service.</param>
        /// <param name="loggingService">The logging service.</param>
        public FileEncryptClient(
            IApiConnectorService apiConnectorService,
            ILoggingService loggingService)
            : base(loggingService, new JsonMediaTypeFormatter())
        {
            this.apiConnectorService = apiConnectorService;

            this.baseUrl = string.Format("{0}/api/file/", this.apiConnectorService.WebAPIUrl.TrimEnd('/'));
        }

        #endregion Constructors

        public async Task<NetworkResponse<AesEncryptResponse>> AesEncryptAsync(string textToEncrypt, string key)
        {
            var requestUri = new Uri($"{this.baseUrl}AesEncrypt");
            var content = new AesEncryptRequest
            {
                PrivateKey = key,
                TextToEncrypt = textToEncrypt
            };
            var request = this.GetApiRequest(HttpMethod.Post, requestUri, content);
            return await this.GetNetworkResponseAsync<AesEncryptResponse>(request).ConfigureAwait(false);
        }

        public async Task<NetworkResponse<AesGenerateKeyResponse>> AesGenerateKeyAsync()
        {
            var requestUri = new Uri($"{this.baseUrl}AesNewKey");
            var request = this.GetApiRequest(HttpMethod.Get, requestUri);
            return await this.GetNetworkResponseAsync<AesGenerateKeyResponse>(request).ConfigureAwait(false);
        }

        public async Task<NetworkResponse<RsaDecryptResponse>> RsaDecryptAsync(string cipherText, bool usePrivateKey)
        {
            var requestUri = new Uri($"{this.baseUrl}RsaDecrypt");
            var content = new RsaDecryptRequest
            {
                UsePrivateKey = usePrivateKey,
                CipherText= cipherText
            };
            var request = this.GetApiRequest(HttpMethod.Post, requestUri, content);
            return await this.GetNetworkResponseAsync<RsaDecryptResponse>(request).ConfigureAwait(false);
        }

        public async Task<NetworkResponse<RsaEncryptResponse>> RsaEncryptAsync(string textToEncrypt, bool usePrivateKey)
        {
            var requestUri = new Uri($"{this.baseUrl}RsaEncrypt");
            var content = new RsaEncryptRequest
            {
                UsePrivateKey = usePrivateKey,
                TextToEncrypt = textToEncrypt
            };
            var request = this.GetApiRequest(HttpMethod.Post, requestUri, content);
            return await this.GetNetworkResponseAsync<RsaEncryptResponse>(request).ConfigureAwait(false);
        }

        public async Task<NetworkResponse<RsaGenerateExtensionResponse>> RsaGenerateExtensionAsync()
        {
            var requestUri = new Uri($"{this.baseUrl}RsaGenerateExt");
            var request = this.GetApiRequest(HttpMethod.Get, requestUri);
            return await this.GetNetworkResponseAsync<RsaGenerateExtensionResponse>(request).ConfigureAwait(false);
        }

        protected override HttpRequestMessage GetApiRequest(HttpMethod method, Uri requestUri)
        {
            var request = new HttpRequestMessage(method, requestUri);

            return request;
        }
    }
}
