﻿namespace CQC.IronBridge.Clients
{
    /// <summary>
    /// Represents the high level information about the network response status.
    /// </summary>
    public enum NetworkResponseStatus
    {
        /// <summary>
        /// The success.
        /// </summary>
        Success,

        /// <summary>
        /// The authentication error.
        /// </summary>
        AuthenticationError,

        /// <summary>
        /// The communication error.
        /// </summary>
        CommunicationError,

        /// <summary>
        /// The web service error.
        /// </summary>
        WebServiceError,

        /// <summary>
        /// The deserialization error.
        /// </summary>
        DeserializationError,

        /// <summary>
        /// The serialization error.
        /// </summary>
        SerializationError
    }
}
