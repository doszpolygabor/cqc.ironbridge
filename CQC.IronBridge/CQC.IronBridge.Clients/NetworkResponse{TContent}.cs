﻿namespace CQC.IronBridge.Clients
{
    using System;
    using System.Net;

    /// <summary>
    /// Represents the network response returning the content of the specified type.
    /// </summary>
    /// <typeparam name="TContent">The type of the content.</typeparam>
    public class NetworkResponse<TContent> : NetworkResponse, IDisposable
        where TContent : class
    {
        #region Fields

        /// <summary>
        /// A value indicating whether this instance has been disposed.
        /// </summary>
        private bool isDisposed;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkResponse{TContent}" /> class.
        /// </summary>
        /// <param name="isSuccessful">The is successful.</param>
        /// <param name="statusCode">The status code.</param>
        /// <param name="status">The status.</param>
        /// <param name="statusPhrase">The status phrase.</param>
        /// <param name="content">The content.</param>
        public NetworkResponse(
            bool isSuccessful,
            HttpStatusCode statusCode,
            NetworkResponseStatus status,
            string statusPhrase,
            TContent content)
            : base(isSuccessful, statusCode, status, statusPhrase)
        {
            this.Content = content;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets the content.
        /// </summary>
        /// <value>The content.</value>
        public TContent Content { get; private set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (this.isDisposed)
            {
                return;
            }

            if (disposing)
            {
                var response = this.Content as IDisposable;
                if (response != null)
                {
                    response.Dispose();
                    this.Content = null;
                }
            }

            this.isDisposed = true;
        }

        #endregion Methods
    }
}
